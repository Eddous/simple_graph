import tkinter as tk
import math
from tkinter import Canvas
from colorsys import hls_to_rgb
import random

outerMargin = 75
innerMargin = 50
defaultWidth = 960
defaultHeight = 520
background = "#dddddd"

pointRadius = 3
lineThickness = 3

saturation = 1
luminosity = 0.3

pixelsPerLine = 20
lenOfLine = 6  # radius
horizontalTextOffset = 20
verticalTextOffset = 30

legendPointR = 6


class Points:
    def __init__(self, style, color):
        self.values = []  # y
        self.color = color
        self.style = style


class Graph:
    def __init__(self, width=defaultWidth, height=defaultHeight, expectedPoints=2, expectedColors=3):
        self.root = tk.Tk()
        self.root.geometry(str(width) + "x" + str(height))
        self.root.resizable(width=False, height=False)
        self.canvas = Canvas(self.root, width=width, height=height, background=background, highlightthickness=0)
        self.canvas.place(x=0, y=0)
        self.height = height
        self.width = width

        self.minY = math.inf
        self.maxY = -math.inf
        self.maxX = expectedPoints - 1

        self.xa = None
        self.xb = None
        self.ya = None
        self.yb = None

        self.redraw = False
        self.points = dict()

        self.colors = self.__generateNewColors(expectedColors)

        self.xLines = (width - 2 * outerMargin - 2 * innerMargin) // pixelsPerLine
        self.yLines = (height - 2 * outerMargin - innerMargin) // pixelsPerLine

        self.__redrawRest()
        self.update()

    def freeze(self):
        self.root.mainloop()

    def update(self):
        if self.redraw:
            self.__calculateViewConsts()
            self.redraw = False
            self.canvas.delete("all")
            self.__redrawRest()
            self.__redrawTrends()
            self.__redrawLegend()
        self.root.update_idletasks()
        self.root.update()

    ################################
    #           COLORS             #
    ################################
    @staticmethod
    def __generateNewColors(expectedColors):
        lis = []
        offset = random.random()
        for i in range(expectedColors):
            hue = (i * 1 / expectedColors) + offset
            if hue > 1:
                hue -= 1
            lis.append(hls_to_rgb(hue, luminosity, saturation))
        return lis

    ################################
    #            MATH              #
    ################################
    @staticmethod
    def __map(r, s, t, u):
        if s == r:
            return 0, (t + u) // 2
        a = (u - t) / (s - r)
        b = t - a * r
        return a, b

    def __calculateViewConsts(self):
        self.xa, self.xb = self.__map(
            0,
            self.maxX,
            outerMargin + innerMargin,
            self.width - outerMargin - innerMargin)
        self.ya, self.yb = self.__map(
            self.minY,
            self.maxY,
            self.height - outerMargin - innerMargin,
            outerMargin)

    def __toPixels(self, x, y):
        return (self.xa * x + self.xb, self.ya * y + self.yb)

    ################################
    #           DRAWING            #
    ################################
    def __circle(self, x, y, r, color):
        self.canvas.create_oval(
            x - r,
            y - r,
            x + r,
            y + r,
            fill=color)

    def __line(self, key, x1, y1, x2, y2):
        self.canvas.create_line(x1, y1, x2, y2, fill=self.points[key].color, width=lineThickness)

    # every point have to be in some trend, single points doesnt exists
    # Dont check if key exists because these are private functions
    def __drawPoint(self, key, index):
        x, y = self.__toPixels(index, self.points[key].values[index])
        if self.points[key].style == "line":
            if index != 0:
                prev_x, prev_y = self.__toPixels(index - 1, self.points[key].values[index - 1])
                self.__line(key, prev_x, prev_y, x, y)
        elif self.points[key].style == "points":
            self.__circle(x, y, pointRadius, self.points[key].color)
        else:
            raise Exception("IDK what style is:", self.points[key].style)

    def __horizontalAxis(self):
        for num in range(0, self.maxX, math.ceil(self.maxX / self.xLines)):
            self.__drawHorizontalLineAndNumber(num)
        self.__drawHorizontalLineAndNumber(self.maxX)

    def __drawHorizontalLineAndNumber(self, num):
        x, _ = self.__toPixels(num, 0)
        y = self.height - outerMargin
        self.canvas.create_line(x, y - lenOfLine, x, y + lenOfLine + 1)
        self.canvas.create_text(x, y + horizontalTextOffset, text=str(num), angle=-45)

    def __verticalAxis(self):
        start = math.ceil(self.minY)
        stop = math.floor(self.maxY)
        step = math.ceil((abs(start) + abs(stop)) / self.yLines)
        self.__drawVerticalLineAndNumber(stop)
        if step == 0:
            return
        for num in range(start, stop, step):
            self.__drawVerticalLineAndNumber(num)

    def __drawVerticalLineAndNumber(self, num):
        _, y = self.__toPixels(0, num)
        x = outerMargin
        self.canvas.create_line(x - lenOfLine, y, x + lenOfLine + 1, y)
        self.canvas.create_text(x - verticalTextOffset, y, text=str(num))

    def __redrawRest(self):
        # vertical line
        self.canvas.create_line(
            outerMargin,
            outerMargin,
            outerMargin,
            self.height - innerMargin - outerMargin)
        # horizontal line
        self.canvas.create_line(
            outerMargin + innerMargin,
            self.height - outerMargin,
            self.width - outerMargin - innerMargin,
            self.height - outerMargin)

    def __redrawTrends(self):
        self.__horizontalAxis()
        self.__verticalAxis()
        for key in self.points:
            for i in range(len(self.points[key].values)):
                self.__drawPoint(key, i)

    def __redrawLegend(self):
        textX = (self.width + self.width - outerMargin) // 2
        y = outerMargin
        for key in self.points:
            self.__circle(self.width - outerMargin, y, legendPointR, self.points[key].color)
            self.canvas.create_text(textX, y, text=key)
            y += pixelsPerLine

    def addTrend(self, key, style, color=None):
        if color == None:
            if len(self.colors) == 0:
                color = "#000000"
            else:
                color = self.colors.pop(random.randint(0, len(self.colors) - 1))
                color = "#" + ('%02x%02x%02x' % (int(color[0] * 255), int(color[1] * 255), int(color[2] * 255)))
        self.points[key] = Points(style, color)

    def addPoint(self, key, y):
        if not key in self.points:
            raise Exception("Trend doesnt exists")
        self.points[key].values.append(y)
        self.redraw = self.redraw or y < self.minY or self.maxY < y or self.maxX < len(self.points[key].values) - 1
        self.minY = min(y, self.minY)
        self.maxY = max(y, self.maxY)
        self.maxX = max(len(self.points[key].values) - 1, self.maxX)
        if self.redraw:
            return
        self.__drawPoint(key, len(self.points[key].values) - 1)

    def prolongX(self, expectedPoints):
        self.redraw = self.maxX < expectedPoints-1
        self.maxX = max(expectedPoints-1, self.maxX)

    def plot(self, key, lis, style, color=None):
        self.addTrend(key, color=color, style=style)
        for val in lis:
            self.addPoint(key, val)
