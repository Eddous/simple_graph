from collections import deque


class MovingAvarage:
    def __init__(self, numberOfPoints):
        self.memory = deque()
        self.numberOfPoints = numberOfPoints
        self.sum = 0

    def addNum(self, num):
        self.sum += num
        self.memory.append(num)
        if len(self.memory) > self.numberOfPoints:
            self.sum -= self.memory.popleft()
        return self.sum / len(self.memory)

def movingAvarage(values, period):
    new = []
    s = 0  # included
    e = 0  # excluded
    summ = 0
    for val in values:
        summ += val
        e += 1
        if e - s > period:
            summ -= values[s]
            s += 1
        new.append(summ / (e - s))
    return new

